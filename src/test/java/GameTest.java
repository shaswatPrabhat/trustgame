import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.PrintStream;
import java.util.Scanner;

public class GameTest {
    @Mock
    PrintStream printStream;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void basicMockGame() {
        Player player1 = new Player(new ConsolePlayerBehaviour(new Scanner("0\n0\n0\n")));
        Player player2 = new Player(new CooperatePlayerBehaviour());
        Machine machine = new Machine();
        PrintStream printStream = new PrintStream(System.out);
        Game game = new Game(player1, player2, machine, printStream);
        game.setNumberOfRounds(3);
        Assert.assertArrayEquals(new int[]{9, -3}, game.play());

    }

    @Test
    public void basicMockGameCheat() {
        Player player1 = new Player(new ConsolePlayerBehaviour(new Scanner("1\n1\n1\n")));
        Player player2 = new Player(new CheatPlayerBehaviour());
        Machine machine = new Machine();
        PrintStream printStream = new PrintStream(System.out);
        Game game = new Game(player1, player2, machine, printStream);
        game.setNumberOfRounds(3);
        Assert.assertArrayEquals(new int[]{-3, 9}, game.play());

    }

    @Test
    public void basicMockGameWithMockPrint() {
        Player player1 = new Player(new CooperatePlayerBehaviour());
        Player player2 = new Player(new ConsolePlayerBehaviour(new Scanner("0\n0\n1\n")));
        Machine machine = new Machine();
        Game game = new Game(player1, player2, machine, printStream);
        game.setNumberOfRounds(1);
        game.play();
        Mockito.verify(printStream).print("\nPlayer1 is making move");
        Mockito.verify(printStream).print("\nPlayer1 moved:COOPERATE");
    }
    @Test
    public  void basicMockAlwaysCoolAndCheat(){
        PrintStream printStream = new PrintStream(System.out);
        Player coolPlayer = new Player(new CooperatePlayerBehaviour());
        Player cheatPlayer = new Player(new CheatPlayerBehaviour());
        Machine machine = new Machine();
        Game game = new Game(coolPlayer, cheatPlayer, machine, printStream);
        game.setNumberOfRounds(5);
        game.play();
    }
    @Test
    public void basicMockShouldReturnCopyCatCopyCat(){
        PrintStream printStream = new PrintStream(System.out);
        Player copyCatPlayer1 = new Player(new CopyCatPlayerBehaviour((MoveType.Moves.COOPERATE)));
        CopyCatPlayerBehaviour copyCatPlayerBehaviour = new CopyCatPlayerBehaviour(MoveType.Moves.COOPERATE);
        Player copyCatPlayer = new Player(copyCatPlayerBehaviour);
        Machine machine = new Machine();
        Game game = new Game(copyCatPlayer1, copyCatPlayer, machine, printStream);
        copyCatPlayerBehaviour.register(game,copyCatPlayer);
        copyCatPlayerBehaviour.register(game,copyCatPlayer1);
        game.setNumberOfRounds(3);
        game.play();
    }

    @Test
    public void basicMockShouldReturnOpponentsInSecondRound(){
        PrintStream printStream = new PrintStream(System.out);
        Player consolePlayer = new Player(new ConsolePlayerBehaviour(new Scanner("1\n2\n2\n")));
        CopyCatPlayerBehaviour copyCatPlayerBehaviour = new CopyCatPlayerBehaviour(MoveType.Moves.COOPERATE);
        Player copyCatPlayer = new Player(copyCatPlayerBehaviour);
        Machine machine = new Machine();
        Game game = new Game(consolePlayer, copyCatPlayer, machine, printStream);
        copyCatPlayerBehaviour.register(game,copyCatPlayer);
        game.setNumberOfRounds(3);
        game.play();
    }

}

