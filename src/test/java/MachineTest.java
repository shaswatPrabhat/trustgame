import org.junit.Assert;
import org.junit.Test;

public class MachineTest {

    private Machine machine = new Machine();

    @Test
    public void shouldReturn00onCheatCheat() {
        int[] expectedScore = {0, 0};
        int[] actualScore = machine.calculate(MoveType.Moves.CHEAT, MoveType.Moves.CHEAT);
        Assert.assertArrayEquals(expectedScore, actualScore);
    }

    @Test
    public void shouldReturn3minus1onCheatCooperate() {
        int[] expectedScore = {3, -1};
        int[] actualScore = machine.calculate(MoveType.Moves.CHEAT, MoveType.Moves.COOPERATE);
        Assert.assertArrayEquals(expectedScore, actualScore);
    }
    @Test
    public void shouldReturnMinus1and3onCooperateCheat() {
        int[] expectedScore = {-1, 3};
        int[] actualScore = machine.calculate(MoveType.Moves.COOPERATE, MoveType.Moves.CHEAT);
        Assert.assertArrayEquals(expectedScore, actualScore);
    }
    @Test
    public void shouldReturn2and2onCooperateCooperate() {
        int[] expectedScore = {2, 2};
        int[] actualScore = machine.calculate(MoveType.Moves.COOPERATE, MoveType.Moves.COOPERATE);
        Assert.assertArrayEquals(expectedScore, actualScore);
    }

}
