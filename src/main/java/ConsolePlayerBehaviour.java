import java.util.Scanner;

public class ConsolePlayerBehaviour implements PlayerBehaviour {
    Scanner scanner;

    public ConsolePlayerBehaviour(Scanner scanner) {
        this.scanner = scanner;
    }
    public MoveType.Moves makeAMove() {
        MoveType.Moves move;
        String input = scanner.nextLine();
        if (Integer.parseInt(input) == 0){
            move= MoveType.Moves.CHEAT;
        }else if(Integer.parseInt(input) == 1) {
            move= MoveType.Moves.COOPERATE;
        }else{
            move= MoveType.Moves.INVALID;
        }
        return move;
    }

}
