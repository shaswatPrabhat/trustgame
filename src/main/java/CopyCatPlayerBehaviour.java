public class CopyCatPlayerBehaviour implements PlayerBehaviour {
    private MoveType.Moves impendingMove = MoveType.Moves.COOPERATE;
    private MoveType.Moves updatedOpponentLastMove = MoveType.Moves.COOPERATE;

    public CopyCatPlayerBehaviour(MoveType.Moves updatedOppoentLastMove) {
//        this.updatedOppoentLastMove = updatedOppoentLastMove;

    }

    @Override
     public MoveType.Moves makeAMove(){
        MoveType.Moves moveToBeMade = impendingMove;
        impendingMove= updatedOpponentLastMove;
        return moveToBeMade;
    }

    public void setUpdatedOpponentLastMove(MoveType.Moves updatedOpponentLastMove) {
        this.updatedOpponentLastMove = updatedOpponentLastMove;
    }
    public void register(Game game, Player player){
        game.registeredPlayers.add(player);

    }
}
